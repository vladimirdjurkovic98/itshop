<!DOCTYPE html>
<html lang="en">

<head>
    <link href='https://fonts.googleapis.com/css?family=Muli' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/bfd89454f4.js" crossorigin="anonymous"></script>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="StyleFirstPage.css">
    <title>Blog</title>
</head>

<body>
    <nav class="navbar">
        <div class="container">
            <section class="navleftsection">
                <a href="">Shipping</a>
                <a href="">FAQ</a>
                <a href="contact.php">Contact</a>
                <a href="">Order</a>
            </section>
            <section class="navrightsection">
                <a href=""><i class="fa fa-check"></i> Free 30 Day Money Back Guarantee</i></a>
                <a href="">USD <i class="fa fa-chevron-down"></i></a>
                <a href="">English <i class="fa fa-chevron-down"></i></a>
            </section>
        </div>
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-3">

                  <a href="index.php">  <img src="pictures/quantox.png" alt="" class="logo" style="width:100px;height: 100;"> </a>

            </div>
            <div class="col-2">
                <section class="lefttext">
                    <h6><b>Send us a message</b></h6><br>
                    <p class="mail">demo@demo.com</p>
                </section>
            </div>
            <div class="col-4">
                <section class="righttext">
                    <h6>Need Help? Call us:<br>
                        <b class="number">012 345 6789</b>
                    </h6>
                </section>
            </div>
            <div class="col-3">
                <section class="icons">
                    <i class="fa fa-user"></i>
                    <i class="fa fa-heart"></i>
                    <i class="fa fa-shopping-bag"></i>
                </section>
            </div>
        </div>
    </div>
    <hr>
    <nav>
        <div class="container">
            <div class="row">
                <div class="col-9">
                    <section class="centersection">
                        <button type="button" class="btn btn-danger">BROWSE CATEGORIES<i class="fa fa-grip-lines"></i></button>
                        <a href="index.php">HOME <i class="fa fa-chevron-down"></i></a>
                        <a href="">SHOP <i class="fa fa-chevron-down"></i></a>
                        <a href="">PAGES <i class="fa fa-chevron-down"></i></a>
                        <a href="blogs.php">BLOGS</a>
                        <a href="contact.php">CONTACT</a>
                    </section>
                </div>
                <div class="col-3">
                    <div class="input-group rounded">
                        <input type="search" class="form-control rounded" placeholder="Search" aria-label="Search" aria-describedby="search-addon" />
                        <span class="input-group-text border-0" id="search-addon">
                            <i class="fas fa-search"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <div class="background">
        <div class="container">
            <b>
                <h4 class="text1">Drop us a line
            </b></h4>
            <p style="text-align: left;" class="text2">Have a question or comment? Use the form below to send us a message or contact by mail</p>
        </div>

</body>

</html>