<!DOCTYPE html>
<html lang="en">

<head>
    <link href='https://fonts.googleapis.com/css?family=Muli' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/bfd89454f4.js" crossorigin="anonymous"></script>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="StyleFirstPage.css">
    <title>Contact</title>
</head>

<body>
    <nav class="navbar">
        <div class="container">
            <section class="navleftsection">
                <a href="">Shipping</a>
                <a href="">FAQ</a>
                <a href="contact.php">Contact</a>
                <a href="">Order</a>
            </section>
            <section class="navrightsection">
                <a href=""><i class="fa fa-check"></i> Free 30 Day Money Back Guarantee</i></a>
                <a href="">USD <i class="fa fa-chevron-down"></i></a>
                <a href="">English <i class="fa fa-chevron-down"></i></a>
            </section>
        </div>
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-3">

            <a href="index.php">   <img src="pictures/quantox.png" alt="" class="logo" style="width:100px;height: 100;"> </a>

            </div>
            <div class="col-2">
                <section class="lefttext">
                    <h6><b>Send us a message</b></h6><br>
                    <p class="mail">demo@demo.com</p>
                </section>
            </div>
            <div class="col-4">
                <section class="righttext">
                    <h6>Need Help? Call us:<br>
                        <b class="number">012 345 6789</b>
                    </h6>
                </section>
            </div>
            <div class="col-3">
                <section class="icons">
                    <i class="fa fa-user"></i>
                    <i class="fa fa-heart"></i>
                    <i class="fa fa-shopping-bag"></i>
                </section>
            </div>
        </div>
    </div>
    <hr>
    <nav>
        <div class="container">
            <div class="row">
                <div class="col-9">
                    <section class="centersection">
                        <button type="button" class="btn btn-danger">BROWSE CATEGORIES<i class="fa fa-grip-lines"></i></button>
                        <a href="index.php">HOME <i class="fa fa-chevron-down"></i></a>
                        <a href="">SHOP <i class="fa fa-chevron-down"></i></a>
                        <a href="">PAGES <i class="fa fa-chevron-down"></i></a>
                        <a href="blogs.php">BLOGS</a>
                        <a href="contact.php">CONTACT</a>
                    </section>
                </div>
                <div class="col-3">
                    <div class="input-group rounded">
                        <input type="search" class="form-control rounded" placeholder="Search" aria-label="Search" aria-describedby="search-addon" />
                        <span class="input-group-text border-0" id="search-addon">
                            <i class="fas fa-search"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <div class="background">
        <div class="container">
            <iframe style="margin: 20px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3020.024603099647!2d-73.96763018398916!3d40.80545313982862!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c2f63c75af2d65%3A0xf45542f6cb090cc9!2s2880%20Broadway%2C%20New%20York%2C%20NY%2010025%2C%20USA!5e0!3m2!1sen!2srs!4v1649086096213!5m2!1sen!2srs" width="1100" height="720" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>

        </div>
        <b>
            <h4 class="text1">Drop us a line
        </b></h4>
        <p style="text-align: left;" class="text2">Have a question or comment? Use the form below to send us a message
            or contact by mail</p>
        <div class="col-5">
            <div class="input-group input">

                <select name="cars" id="cars">

                    <option value="Customer service">Customer service</option>
                    <option value="volvo">Volvo</option>
                    <option value="saab">Saab</option>
                    <option value="mercedes">Mercedes</option>
                    <option value="audi">Audi</option>
                </select>


                <input type="email" id="mail" placeholder="Your email address" style="height: 3rem; width: 250px;"><br>

                <input type="file" id="avatar" name="avatar" accept="image/png, image/jpeg">


                <textarea style="color:gray;" id="text11" name="w3review" rows="4" cols="50"></textarea>

                <button type="button" onclick="ValidateEmail(); ValidateText()" class="btn btn-danger btn2">Post comment </button>
            </div>
        </div>
    </div>

    </div>
    <div class="black">
        <div class="container">
            <div class="row">
                <div class="col-1.5">
                    <img src="slike/slika4.png" alt="" class="postcard">
                </div>
                <div class="col-2.5">
                    <b>
                        <h4 class="posttext">Fear Of Missing Out?
                    </b></h4>
                    <p style="color: gray;" class="posttext">Get the latest deals, updates and more.</p>
                </div>
                <div class="col-5">
                    <div class="input-group input">
                        <input type="search" class="form-control rounded" placeholder="Your email address" aria-label="Search" aria-describedby="search-addon" style="height: 3rem;">
                        <button type="button" class="btn btn-danger btn2">Subscribe </button>
                    </div>
                </div>
                <div class="col-3 media">
                    FOLLOW US
                    <i class="fa fa1 fa-facebook-f"></i>
                    <i class="fa fa1 fa-twitter"></i>
                    <i class="fa fa1 fa-instagram"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="container details">
        <div class="row">
            <div class="col-2">
                <h3 style="line-height: 10rem;">COMPANY</h3>
                <p>About us</p>
                <p>Careers</p>
                <p>Affiliates</p>
                <p>Blog</p>
                <p>Contact Us</p>
            </div>
            <div class="col-2">
                <h3 style="line-height: 10rem;">SHOP</h3>
                <p>Televisions</p>
                <p>Fridges</p>
                <p>Washing Machines</p>
                <p>Fans</p>
                <p>Air Conditioners</p>
                <p>Laptops</p>
            </div>
            <div class="col-2">
                <h3 style="line-height: 10rem;">HELP</h3>
                <p>Customers Service</p>
                <p>My Account</p>
                <p>Find a Store</p>
                <p>Legal & Privacy</p>
                <p>Contact</p>
                <p>Gift Card</p>
            </div>
            <div class="col-2">
                <h3 style="line-height: 10rem;">ACCOUNT</h3>
                <p>My Profile</p>
                <p>My Order History</p>
                <p>My Wish List</p>
                <p>Order Tracking</p>
            </div>
            <div class="col-4 contactinfo">
                <h3 style="line-height: 10rem;">CONTACT INFO</h3>
                <p>About us</p>
                <p>Contact us</p>
                <p>Blog</p>
            </div>
        </div>
    </div>
    <hr>


    <hr>
    Copyright &copy; 2022 Technolgy. All rights reserved
    </div>
</body>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>

<script src="validation.js"></script>

</html>