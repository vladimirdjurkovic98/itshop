<!DOCTYPE html>
<html lang="en">

<head>
    <link href='https://fonts.googleapis.com/css?family=Muli' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css"
        integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/bfd89454f4.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="StyleFirstPage.css">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <nav class="navbar">
        <div class="container">
            <section class="navleftsection">
                <a href="">Shipping</a>
                <a href="">FAQ</a>
                <a href="">Contact</a>
                <a href="">Order</a>
            </section>
            <section class="navrightsection">
                <a href=""><i class="fa fa-check"></i> Free 30 Day Money Back Guarantee</i></a>
                <a href="">USD <i class="fa fa-chevron-down"></i></a>
                <a href="">English <i class="fa fa-chevron-down"></i></a>
            </section>
        </div>
    </nav>
    <div class="container">
        <div class="row">
            <div class="col-3">
               <a href="index.php"> <img src="pictures/quantox.png" alt="" class="logo" style="width:100px;height: 100;"> </a>
            </div>
            <div class="col-2">
                <section class="lefttext">
                    <h6><b>Send us a message</b></h6><br>
                    <p class="mail">demo@demo.com</p>
                </section>
            </div>
            <div class="col-4">
                <section class="righttext">
                    <h6>Need Help? Call us:<br>
                        <b class="number">012 345 6789</b>
                    </h6>
                </section>
            </div>
            <div class="col-3">
                <section class="icons">
                    <i class="fa fa-user"></i>
                    <i class="fa fa-heart"></i>
                    <i class="fa fa-shopping-bag"></i>
                </section>
            </div>
        </div>
    </div>
    <hr>
    <nav>
        <div class="container">
            <div class="row">
                <div class="col-9">
                    <section class="centersection">
                        <button type="button" class="btn btn-danger">BROWSE CATEGORIES<i
                                class="fa fa-grip-lines"></i></button>
                        <a href="index.php">HOME <i class="fa fa-chevron-down"></i></a>
                        <a href="">SHOP <i class="fa fa-chevron-down"></i></a>
                        <a href="">PAGES <i class="fa fa-chevron-down"></i></a>
                        <a href="blogs.php">BLOGS</a>
                        <a href="contact.php">CONTACT</a>
                    </section>
                </div>
                <div class="col-3">
                    <div class="input-group rounded">
                        <input type="search" class="form-control rounded" placeholder="Search" aria-label="Search"
                            aria-describedby="search-addon" />
                        <span class="input-group-text border-0" id="search-addon">
                            <i class="fas fa-search"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <div class="background">
        <div class="container">
            <section class="images">
                <div class="item-a">
                    <img class=""
                        src="pictures/slika1.jpg"
                        alt=""
                        style="width:600px;height: 760px;">
                        
                </div>
                <div class="item-b">
                    <img src="Pictures/slika2.jpg"
                        alt=""
                        style="width:600px;height: 360px;">
                </div>
                <div class="item-c">
                    <img src="pictures/slika3.jpg"
                        alt=""
                        style="width:600px;height: 360px;">
                        
                </div>
            </section>
        </div>
        <div class="container">
            <section class="text">
                <b>
                    <h4 class="text">Our Featured Offers
                </b></h4>
                <p class="text text2" style="text-align:right;">View all></p>
            </section>
        </div>
        <div class="container">
            <div id="content">
                <img src="pictures/slika1.jpg"
                    alt="pic1">
                <img src="pictures/slika1.jpg"
                    alt="pic1">
                <img src="pictures/slika1.jpg"
                    alt="pic1">
                <img src="pictures/slika1.jpg"
                    alt="pic1">

            </div>
            <section class="text1">
                <b>
                    <h4 class="text1">This Weak Deals
                </b></h4>
                <p class="text1 text2">View all></p>
            </section>
            <div id="content1">
                <img src="pictures/slika2.jpg"
                    alt="pic1"
                    style="width: 520px;">
                <img src="pictures/slika2.jpg"
                    alt="pic1"
                    style="width: 520px;">
            </div>
        </div>
    </div>

    <div class="container" style="margin-top: 4rem;">
        <div class="row">
            <div class="col-2">
                <img src="pictures/quantox.png" alt="" class="png">
            </div>
            <div class="col-2">
                <img src="pictures/quantox.png" alt="" class="png">
            </div>
            <div class="col-2">
                <img src="pictures/quantox.png" alt="" class="png">
            </div>
            <div class="col-2">
                <img src="pictures/quantox.png" alt="" class="png">
            </div>
            <div class="col-2">
                <img src="pictures/quantox.png" alt="" class="png">
            </div>
            <div class="col-2">
                <img src="pictures/quantox.png" alt="" class="png">
            </div>
        </div>
    </div>
    <div class="black">
        <div class="container">
            <div class="row">
                <div class="col-1.5">
                    <img src="slika4.png" alt="" class="postcard">
                </div>
                <div class="col-2.5">
                    <b>
                        <h4 class="posttext">Fear Of Missing Out?
                    </b></h4>
                    <p style="color: gray;" class="posttext">Get the latest deals, updates and more.</p>
                </div>
                <div class="col-5">
                    <div class="input-group input">
                        <input type="search" class="form-control rounded" placeholder="Your email address"
                            aria-label="Search" aria-describedby="search-addon" style="height: 3rem;">
                        <button type="button" class="btn btn-danger btn2">Subscribe </button>
                    </div>
                </div>
                <div class="col-3 media">
                    FOLLOW US
                    <i class="fa fa1 fa-facebook-f"></i>
                    <i class="fa fa1 fa-twitter"></i>
                    <i class="fa fa1 fa-instagram"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="container details">
        <div class="row">
            <div class="col-2">
                <h3 style="line-height: 10rem;">COMPANY</h3>
                <p>About us</p>
                <p>Careers</p>
                <p>Affiliates</p>
                <p>Blog</p>
                <p>Contact Us</p>
            </div>
            <div class="col-2">
                <h3 style="line-height: 10rem;">SHOP</h3>
                <p>Televisions</p>
                <p>Fridges</p>
                <p>Washing Machines</p>
                <p>Fans</p>
                <p>Air Conditioners</p>
                <p>Laptops</p>
            </div>
            <div class="col-2">
                <h3 style="line-height: 10rem;">HELP</h3>
                <p>Customers Service</p>
                <p>My Account</p>
                <p>Find a Store</p>
                <p>Legal & Privacy</p>
                <p>Contact</p>
                <p>Gift Card</p>
            </div>
            <div class="col-2">
                <h3 style="line-height: 10rem;">ACCOUNT</h3>
                <p>My Profile</p>
                <p>My Order History</p>
                <p>My Wish List</p>
                <p>Order Tracking</p>
            </div>
            <div class="col-4 contactinfo">
                <h3 style="line-height: 10rem;">CONTACT INFO</h3>
                <p>About us</p>
                <p>Contact us</p>
                <p>Blog</p>
            </div>
        </div>
    </div>
    <hr>
    <div class="container">
        <div class="row">
            <div class="col-3">
                <i class="fa fa-truck fa-3x"></i>
            </div>
            <div class="col-3">
                <i class="fa fa-truck fa-3x"></i>
            </div>
            <div class="col-3">
                <i class="fa fa-truck fa-3x"></i>
            </div>
            <div class="col-3">
                <i class="fa fa-truck fa-3x"></i>
            </div>

        </div>

        <hr>
        Copyright &copy; 2022 Technolgy. All rights reserved
    </div>
</body>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
    integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
    crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF"
    crossorigin="anonymous"></script>

</html>